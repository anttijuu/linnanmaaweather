# Linnanmaa Weather

**This repository has moved to [https://github.com/anttijuu/LinnanmaaWeather](https://github.com/anttijuu/LinnanmaaWeather)**

The Linnanmaa weather app for Android. A demonstration of HTTP and AsyncTask programming with Java in Android.

Fetches the JSON from http://weather.willab.fi/weather.json and extracts the current temperature, humidity and air pressure from the file.

Original implementation updated with new HTTP APIs after AndroidHttpClient deprecated (Antti Juustila). Also changed XML to JSON by Antti Juustila (2020).

(c) Henrik Hedberg & Antti Juustila
